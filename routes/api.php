<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('/routes', 'Thanu\RouteViewer\Http\Controllers\RouteController@getRoutes');